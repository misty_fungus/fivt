#include <iostream>
#include <condition_variable>
#include <queue>
#include <mutex>
#include <stdexcept>

template <class T, class Container = std::deque<T>>
class BlockingQueue {
 public:
  explicit BlockingQueue(const size_t& capacity);

  // You need to implement Put method which will block and wait while capacity
  // of your container exceeded. It should work with noncopyable objects and
  // use move semantics for placing elements in the container. If the queue is 
  // shutdown you need to throw an exception inherited from std::exceptiono.
  void Put(T&& element);

  // Get should block and wait while the container is empty, if the queue
  // shutdown you will need to return false.
  bool Get(T& element);

  // Shutdown should turn off the queue and notify every thread which is waiting
  // for something to stop wait.
  void Shutdown();
};
