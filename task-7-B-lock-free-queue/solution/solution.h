#pragma once

#include <thread>
#include <atomic>

///////////////////////////////////////////////////////////////////////

template <typename T, template <typename U> class Atomic = std::atomic>
class LockFreeQueue {
    struct Node {
        T element_{};
        Atomic<Node*> next_{nullptr};

        explicit Node(T element, Node* next = nullptr)
            : element_(std::move(element))
            , next_(next) {
        }

        explicit Node() {
        }
    };

 public:
    LockFreeQueue() {
        Node* dummy = new Node{};
        head_ = dummy;
        tail_ = dummy;
    }

    ~LockFreeQueue() {
        // not implemented
    }

    void Enqueue(T /* element */) {
        // not implemented
    }

    bool Dequeue(T& /* element */) {
        // not implemented
        return false;
    }

 private:
    Atomic<Node*> head_{nullptr};
    Atomic<Node*> tail_{nullptr};
};

///////////////////////////////////////////////////////////////////////
