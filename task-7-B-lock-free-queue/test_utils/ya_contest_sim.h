#pragma once

#include "affinity.h"

namespace SolutionTests {

////////////////////////////////////////
// ya.contest single-core env simulation
////////////////////////////////////////

void SimulateYandexContest() {
    AttachThreadsToSingleCore();
}

}
