#pragma once

#include <atomic>
#include <thread>

//////////////////////////////////////////////////////////////////////

template <typename T, size_t YieldFreq = 3>
class FlakyAtomic {
public:
    FlakyAtomic(const T& initial_value)
        : register_(initial_value) {
    }

    T load() {
        Yield();
        const T value = register_.load();
        Yield();
        return value;
    }

    void store(const T& value) {
        Yield();
        register_.store(value);
        Yield();
    }

    FlakyAtomic& operator =(const T& value) {
        store(value);
        return *this;
    }

    T exchange(const T& new_value) {
        Yield();
        const T prev_value = register_.exchange(new_value);
        Yield();
        return prev_value;
    }

    bool compare_exchange_weak(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_weak(expected, desired);
        Yield();
        return succeeded;
    }

    bool compare_exchange_strong(T& expected, const T& desired) {
        Yield();
        const bool succeeded = register_.compare_exchange_strong(expected, desired);
        Yield();
        return succeeded;
    }


    size_t fetch_add(const size_t value) {
        static_assert(std::is_integral<T>::value, "not supported");
        Yield();
        const auto prev_value = register_.fetch_add(value);
        Yield();
        return prev_value;
    }

    size_t fetch_sub(const size_t value) {
        static_assert(std::is_integral<T>::value, "not supported");
        Yield();
        const auto prev_value = register_.fetch_sub(value);
        Yield();
        return prev_value;
    }

    size_t operator ++() {
        return fetch_add(1) + 1;
    }

    size_t operator --() {
        return fetch_sub(1) - 1;
    }

private:
    void Yield() {
        static std::atomic<size_t> called{0};

        if (called.fetch_add(1) % YieldFreq == 0) {
            std::this_thread::yield();
        }
    }

private:
    std::atomic<T> register_;
};

//////////////////////////////////////////////////////////////////////
